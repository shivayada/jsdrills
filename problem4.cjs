const inventory = require('./info.cjs');

function problem4(inventory){
    if(arguments.length < 1  || inventory.length === 0 || !Array.isArray(inventory)){
        return [];
    }
    const lisyears = inventory.map(index => index['car_year']);
    return lisyears;
}

module.exports =  problem4 

