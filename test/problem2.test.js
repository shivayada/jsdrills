const problem2 = require('../problem2.cjs');
const inventory = require('../info.cjs');

test('provide the index of id',() => {
  expect(problem2(inventory)).toStrictEqual({"id":50,"car_make":"Lincoln","car_model":"Town Car","car_year":1999})
})
