
const  inventory = require('./info.cjs');


function problem2(inventory){
    if(arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)){
        return [];
    }
    let last= inventory.length-1;
    return inventory[last];
   

}
module.exports =  problem2

