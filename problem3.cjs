const  inventory = require('./info.cjs');



function problem3(inventory){
    if(arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)){
        return [];
    }
    let lis = inventory.map(index => index['car_model']);
    lis.sort();
    return lis;

}
module.exports = problem3

 