const { default: inventory } = require("./info.cjs");

function problem5(result){
    if (arguments.length < 1 || result.length === 0 || !Array.isArray(result)){
        return 0;
    }
    let cars2k = result.reduce((acc,cur) =>{
        if (cur < 2000){
            acc +=1;
        }
        return acc;
    } ,0);
         

    return cars2k;
}
module.exports = problem5

