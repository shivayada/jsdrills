const inventory = require('./info.cjs');



function problem1(inventory,cid){
    if(arguments.length !== 2 || inventory.length===0 || !Array.isArray(inventory) || typeof cid !== 'number'){
        return [];
    }
    let index = inventory.filter(line => cid ===line.id);
    return index;
   
}

module.exports = problem1 

